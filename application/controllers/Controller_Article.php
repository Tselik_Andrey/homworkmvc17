<?php

class Controller_Article extends Controller
{

    public function action_index()
    {
        $articlesModel = new Model_Article();

        $data = $articlesModel->getArticles(); ///

        $this->view->generate('article_view.php', 'template_view.php', $data);
    }


    public function action_article($number_id)
    {
        $articleId = new Model_Article();

        $data = $articleId->getText($number_id); ///

        $this->view->generate('show_art_view.php', 'template_view.php', $data);
    }



}