<?php

class Controller_Portfolio extends Controller
{

    public function action_index()
    {
        $portfolioModel = new Model_Portfolio();

        $data = $portfolioModel->getWorks();

        $this->view->generate('portfolio_view.php', 'template_view.php', $data);
    }



    public function action_work($number_id)
    {
        $portfolioModel = new Model_Portfolio();

        $data = $portfolioModel->getText($number_id);

        $this->view->generate('show_portfol_view.php', 'template_view.php', $data);
    }


}