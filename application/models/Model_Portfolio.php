<?php


class Model_Portfolio extends Model
{


    public function getWorks() //////////
    {
        $query = "SELECT * FROM portfolio ";
        $stmt = $this->db->query($query);;
        $rows = $stmt->fetchAll();
        if(empty($rows)) {
            return null;
        }
        return $rows;

    }

    public function getText($number_id) //////////
    {
        $query = "SELECT * FROM portfolio where id = $number_id ";
        $stmt = $this->db->query($query);
        $stmt -> bindValue(':id', $number_id);
        $rows_text = $stmt->fetchAll();
        if(empty($rows_text)) {
            return null;
        }
        return $rows_text;

    }

}