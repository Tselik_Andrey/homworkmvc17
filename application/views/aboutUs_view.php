<div class="container">
    <h1>Information about us:</h1>

    <p>You can disable zooming capabilities on mobile
        devices by adding user-scalable=no to the viewport
        meta tag. This disables zooming, meaning users are
        only able to scroll, and results in your site feeling
        a bit more like a native application.
        Overall, we don't recommend this on every site, so use caution! </p>

</div>