<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/main.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>


<body>
     <nav class="navbar navbar-inverse">
     <div class="container">
         <ul class="nav navbar-nav">
             <li> <a href="/">Home</a></li>
             <li> <a href="/Main/aboutUs">About Us</a></li>
             <li> <a href="/Portfolio/index">Portfolio</a></li>
             <li> <a href="/article">Articles</a></li>
         </ul>
     </div>
    </nav>
            <?php include 'application/views/'.$content_view;?>

            <hr>
            <div class="footer">
                <div class="container">
                <div class="col-md-4"><b>Tselik</b></div>
                <div class="col-md-4"><b>Andrey</b></div>
                <div class="col-md-4"><b>Alekseevich</b></div>
            </div>
    </div>
</body>
</html>