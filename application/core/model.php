<?php

/**
 * Created by PhpStorm.
 * User: hillel
 * Date: 5/21/17
 * Time: 4:21 PM
 */
class Model
{
    public $db;

    public function __construct()
    {
        $this->db = $this->getDb();
    }

    public function getDb()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=shop', 'root', '');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec("SET NAMES 'utf8'");

            return $pdo;
        } catch (PDOException $e) {
            echo "Не получилось подключиться к Базе Данных.<br>";
            echo $e->getMessage();
            exit();
        }
    }

}