<?php


class Route
{
    static function start(){

        $controller_name = 'main';

        $action_name = 'index';
        
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // Lets get controller name
        if(!empty($routes[1])){
            $controller_name = $routes[1];
        }

        if(!empty($routes[2])){
            $action_name = $routes[2];
        }

        if(!empty($routes[3])){
            $number_id = $routes[3];
        }

        $model_name = 'Model_'.$controller_name;

        $controller_name = 'Controller_'.$controller_name;

        $action_name = 'action_'.$action_name;

        $model_path = 'application/models/'.$model_name.'.php';

        if(file_exists($model_path)){
            include_once $model_path;
        }

        $controller_path = 'application/controllers/'.$controller_name.'.php';


        if(file_exists($controller_path)){
            include_once $controller_path;
        }else{
            Route::ErrorPage404();
        }

        $controller = new $controller_name; //$controller = new Controller_Main;


        if(method_exists($controller, $action_name)){

            if (isset($number_id)) {

                $controller->$action_name($number_id);//Controller_Main->action_aboutUs($action_id);

            }else {
                $controller->$action_name();
            }
        }else{
            Route::ErrorPage404();
        }

    }


    static function ErrorPage404(){
        die('404');
        //TODO: Make normal 404 page
    }
}