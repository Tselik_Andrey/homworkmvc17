-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 05 2017 г., 18:30
-- Версия сервера: 5.7.16
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(50) NOT NULL,
  `tittle` varchar(255) NOT NULL,
  `texxt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `articless`
--

CREATE TABLE `articless` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articless`
--

INSERT INTO `articless` (`id`, `title`, `text`, `date`) VALUES
(1, 'В украинский парламент могут избираться «идиоты» и «лунатики»\n', 'Prавительство Великобритании выступило с инициативой об отмене все еще действующих в стране средневековых законов, которые запрещают избирать в парламент страны \"лунатиков\" и \"идиотов\".\r\n', '2017-05-08'),
(2, 'Шведы стали чемпионами мира по хоккею\r\n', 'Российская команда стала бронзовым призером, обыграв сборную Финляндии. Матч закончился со счетом 5:3. Лучшим бомбардиром на чемпионате назвали нападающего Артемия Панарина. Также был отмечен вратарь россиян Андрей Василевский.', '2017-05-24'),
(3, 'Хардкис победил в номинации Лучший вокал и занял первое место!  ', 'Director: Valeriy Bebko\r\nEditor: Valeriy Bebko/Valeriy Kozlitinov\r\nDOP: Yura Korol\'\r\nMake up: Slava Chaika & assistant(by Cinecitta)\r\nHair: Vitalik Datsyuk & Nastia Bilan\'\r\nStyle: Slava Chaika & Helen Logvin\r\nVFX/Graphics: Roman Perfilyev, Valeriy Bebko, ', '2017-06-02'),
(4, 'Hardkisss оставил соперников далеко позади набрав 2300 голосов!!', 'Thanks to: Kadim Tarasov, Kristina Stryzhalko, Kristina Tsymlanskaya, Lee, Maia, Andrey, Alena Alenova, Nastia, Denis, Dzen, Sergey, Andrey T., Kate, Denis Khristov, Jackson, Yves, Igor, Avgust, ', '2017-06-03');

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `name_work` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `portfolio`
--

INSERT INTO `portfolio` (`id`, `year`, `url`, `description`, `name_work`) VALUES
(1, '1999', 'https://www.youtube.com/?gl=UA', '<h3 style=\"color: #0b4075\">БЛОГ</h3>– дать каждому возможность узнать больше о мире вокруг и выразить себя.', 'Блог'),
(2, '2001', 'https://www.amazon.com/', '<h3 style=\"color: #0b4075\">ИНТЕРНЕТ МАГАЗИН</h3> to Amazon.com. Amazon Services LLC and/or its affiliates (\"Amazon\") provide website features and other products and services to you when you visit or shop at Amazon.com', 'Интернет магазин'),
(3, '2017', 'https://translate.google.com', '<h3 style=\"color: #0b4075\">СОЦИАЛЬНАЯ СЕТЬ</h3> Uncaught exception \'PDOException\' with message \'SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \':id\' at line 1\' in D:\\OpenServer\\domains\\hwmvc.dev\\application\\models\\Model_Portfolio.php:23 Stack trace: #0 ', 'Социальная сеть'),
(4, '2018', 'http://hwmvc.dev/Portfolio/index', '<h3 style=\"color: #0b4075\">ФАЙЛООБМЕННИК</h3> ошибка: исключить исключение «PDOException» с сообщением «SQLSTATE [42000]: ошибка синтаксиса или нарушение доступа: 1064 У вас есть ошибка в синтаксисе SQL; Проверьте руководство, соответствующее версии вашего сервера MySQL, для правильного синтаксиса для использования рядом с: id в строке 1 в D: \\ OpenServer \\ domains \\ hwmvc.dev \\ application \\ models \\ Model_Portfolio.php: 23 Трассировка стека: # 0 D: \\ OpenServer \\ domains \\ ', 'Файлообменник');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `price` int(50) NOT NULL,
  `discount` varchar(50) DEFAULT NULL,
  `num_pages` int(50) DEFAULT NULL,
  `play_length` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `type`, `title`, `first_name`, `last_name`, `price`, `discount`, `num_pages`, `play_length`) VALUES
(1, 'book', 'Tom Soer', 'Kristian', 'Anderson', 200, '10', 370, NULL),
(2, 'cd', 'Beatles', 'Mick', 'Jager', 300, '50', NULL, 120),
(3, 'sd', 'Nirvana', 'Kurt', 'Cobain', 400, '100', NULL, 150),
(4, 'book', 'Lov in LN', 'Margaret', 'Techer', 50, NULL, 477, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `articless`
--
ALTER TABLE `articless`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `articless`
--
ALTER TABLE `articless`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
